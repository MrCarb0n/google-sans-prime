### 2022.04.0201
* [misc] In-Module updater / changelog
* [omf] updated template - new omf-next
### 2021.12.2701
* fixed modprop
### 2021.12.2601
* [omf] added auto fallback function
### 2021121701
* [code] fontspoof() fixed vlc
### 2021121101
* updated README
* minor changes
### 2021121002
* fixed duplicate vars
### 2021121001
* [omf] added common vars; improved Pixel logic; fixed finish() umount afdko
### 2021120903
* minor changes
### 2021120902
* [omf] added some common vars for font file extensions
### 2021120901
* [config] added STATIC option
### 2021120701
* minor changes
* [code] moved serif installation before monospace
* [omf] added tools for future use
### 2021120604
* updated release
* [omf] optimizations
### 2021120602
* [omf] fontspoof check if there is any static fonts
### 2021120601
* [config] minor changes
* [omf] updated customize.sh
* [omf] added support for afdko
### 2021120501
* [config] minor changes
* [config] minor changes
* [font] reverted monospace metrics change; [config] rearranged some elements
### 2021120401
* [font] applied DroidSansMono metrics to GSMono; [tools] updated bspatch
### 2021120101
* [omf] fallback() minor fix
### 2021112801
* [omf] improved fallback function
### 2021112701
* [omf] improved fallback logic
### 2021112501
* [omf] added falias function
### 2021112403
* minor changes
* [omf] Pixel: added GS_italic option
### 2021112402
* [omf] removed bold() from customize.sh, moved it to install_font()
### 2021112401
* [omf] removed links, made static fonts installation process fully automatic
### 2021112201
* used twrp extension for uninstalling
* minor changes
* [omf] added twrp uninstallation logic
### 2021112103
* [omf] src() allowed to run scripts after rom()
### 2021112102
* [tools][font] updated patch method
### 2021112101
* minor changes
* Merge branch 'master' of gitlab.com:nongthaihoang/google-sans-prime
* minor changes
* Update README.md
* [template] made alias for lato, source-sans-pro
### 2021111803
* changed font spoofing for monospace
### 2021111802
* [template] do not load twrp extension if in Magisk
### 2021111801
* [template] do not load twrp extension if in Magisk
### 2021111703
* [template] always install to system when detect twrp extension; [config] font spoofing
### 2021111702
* [template] fixed LG support
### 2021111701
* [template] fixed LG rom
### 2021111601
* Update README.md
* Update README.md
* Update README.md
* [template] fixed rename function
### 2021111501
* Update README.md
* [config] added some axes info
### 2021111103
* [font][config] added Hebrew option - status bar padding
### 2021111102
* [template] code optimizations
### 2021111101
* [template] code optimizations; improved fallback
### 2021110701
* [template] do not make links if no sans-serif
### 2021110604
* [repo] removed unused files
* [repo] removed unused files
* [template] fixed missing monospace font styles
### 2021110603
* [config] added font specifications, removed ununsed stuffs
### 2021110602
* updated template
### 2021110601
* updated release
* [template] added font family options
### 2021110503
* fixed Pixel no opsz
### 2021110502
* updated config
### 2021110501
* updated template
### 2021110401
* fixed typo
* [template] code optimizations
### 2021110302
* fixed fallback twice
### 2021110301
* updated template
### 2021103101
* minor changes
* [template] improved pixel rom detection
### 2021103001
* updated template
### 2021102601
* template: do not abort if SANS=false
### 2021102301
* updated template
### 2021102201
* added Geometric Shape Unicode code points 25C1, 25B7, 25BD
### 2021101703
* updated template: rom ignores miui a10
### 2021101702
* updated template: rom ignores miui a10
### 2021101701
* updated template: rom ignores miui a10
### 2021101101
* Replaced GS Mono static font with GS Mono VF; updated README: add Mono preview demo
### 2021101001
* added GS Mono
### 2021100801
* updated template: improved support for miui
### 2021100701
* minor changes
* updated template: added support for MIUI
### 2021100501
* updated template: Color OS 11, A12 fontxml
### 2021080201
* updated template: added option to disable GS
### 2021072801
* Update README.md
* updated readme
* updated readme
* Merge branch 'master' of gitlab.com:nongthaihoang/google-sans-prime
* template: read S, M styles
### 2021072401
* Update README.md
* updated readme
* updated readme
* updated images
* updated release
* config: minor improvements
### 2021072303
* config: minor changes in comments
### 2021072302
* updated template
### 2021072301
* updated template
### 2021072205
* fixed rounded always true
### 2021072204
* optimized code
* optimized code
* optimized code
* optimized code
* added opentype feature: tabular figures
### 2021072203
* updated template
### 2021072202
* updated template
* updated template
* updated template: config: swithched from box-drawing to ascii chars
### 2021072201
* Update README.md
* updated release
### 2021072101
* updated temple: config: improved visual.
* updated readme
* updated readme
* Update README.md
* updated template
### 2021072002
* updated readme
* optimized code
* show font name when flashing
* updated template
### 2021072001
* updated readme
* updated readme
* updated readme
* config: brought back other font styles for easy customization
### 2021071901
* updated template: support twrp extension
### 2021071301
* updated readme
* updated readme
* config: rm MS
* new gsvf rounded
### 2021071202
* updated template: source extensions
### 2021071201
* readme: fixed typos
* readme: simplified
* updated template: extensions
### 2021061903
* updated readme
### 2021061902
* updated config; readme - added preview
### 2021061901
* GSVF A12
### 2021060401
* updated template
### 2021060202
* return
### 2021060201
* updated release
* updated template
* updated template
### 2021053001
* updated readme
* updated readme
* updated readme
* updated readme
* updated readme
* updated readme
* Merge branch 'dev' into 'master'
* Merged dev
* updated readme
* updated readme
* updated readme
* updated readme
* updated readme
* Revert "updated readme"
* updated readme
* Merge branch 'dev' into 'master'
* updated release
* Update README.md
* updated release
* updated readme; added axes specs to the config
* switched to rolling release
### v21.05.24
* Removed all fonts
